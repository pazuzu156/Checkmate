# Checkmate

The readme file for Checkmate and build instructions

## Windows Build

There are 2 things you will need to build Checkmate on Windows.

1. [Visual Studio][1] 2017 and up (2019 works)
2. [Qt 5.13][2]

Checkmate is built with Microsoft Visual C++, so you need to install developer packages for it. The easiest way is by installing Visual Studio.

To compile, Simply run `build.bat` You can provide flags for it too

`/> build.bat -b` <- Forces build  
`/> build.bat -c` <- Forces cleaning (clean.bat just runs this)  
`/> build.bat -v <VERSION>` <- Specifies a version for Checkmate (skips version question)

## Linux Build

To build in Linux, the Qt libraries are required. If you have the option to install modules sepearately, the two you will need are:

1. Qt5-Base
2. Qt5-Webkit

To compile checkmate, be sure that you have the build tools for your specific distribution, that includes make and g++

Compile:
```$ make```

Clean up build process
```$ make clean```

Linux also has 2 other commands for the makefile, install and uninstall. Both require root, so be sure to run the install/uninstall commands as root

Install:
```$ make && sudo make install```

Uninstall:
```$ make uninstall```

## Distro Install

### Arch

Checkmate is in the Arch User Repository under [checkmate-git][3]

## Contributors

[Pazuzu156](https://github.com/pazuzu156)

[1]:https://visualstudio.microsoft.com/
[2]:http://www.qt.io/
[3]:https://aur.archlinux.org/packages/checkmate-git/
