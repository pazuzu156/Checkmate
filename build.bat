@echo off
title Checkmate Build Tool
color 0A

set CWD=%~dp0

rem Qt Version to build against
set QT=5.13.0

rem Change to 32 for a 32 bit build
set ARCH=64

rem Gets max processor threads to compile on
set /a NPROC="%NUMBER_OF_PROCESSORS% + 1"

rem Sets architecture specific variables
if %ARCH%==32 (
	set MSVC=msvc2017
	set SETUP=checkmate_i686
	set VCA=x86
	set VCR=vcredist_msvc2017_x86.exe
) else if %ARCH%==64 (
	set MSVC=msvc2017_64
	set SETUP=checkmate_amd64
	set VCA=amd64
	set VCR=vcredist_msvc2017_x64.exe
)

rem Setting system path with needed toolchain locations
set CPATH=%PATH%
set PATH=C:\Qt\%QT%\%MSVC%\bin;C:\Qt\Tools\QtCreator\bin;^
C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build;^
C:\Program Files (x86)\Inno Setup 5;%CPATH%

rem Make sure to get nmake!!!!!
call vcvarsall %VCA%

rem Just in case you wanna run a certain
rem event manually, use one of the cmd args
rem -b force build
rem -c force clean
if "%1" NEQ "" (
	if "%1" EQU "-b" (
		goto build
	) else if "%1" EQU "-c" (
		goto clean
	) else if "%1" EQU "-v" (
		goto build
	) else (
		goto error
	)
)

:build
echo.
if "%1" EQU "-v" (
	set VERSION=%2
) else (
	if "%3" NEQ "" (
		set VERSION=%4
	) else (
		set /p VERSION="What's the version? "
	)
)
echo.
echo building makefile
qmake src\Checkmate.pro -o src\Makefile
echo.
echo compiling binary
cd %CWD%\src
jom /J %NPROC% /F Makefile.release
cd %CWD%
copy /Y src\release\Checkmate.exe "%CWD%\checkmate_win32_bin\bin\Checkmate.exe"
copy /Y "C:\Qt\vcredist\%VCR%" "%CWD%\checkmate_win32_bin\bin\redist\vcredist_msvc2017.exe"
echo.
echo copying required libraries
copy /Y C:\Qt\%QT%\%MSVC%\bin\Qt5Core.dll "%CWD%\checkmate_win32_bin\bin\Qt5Core.dll"
copy /Y C:\Qt\%QT%\%MSVC%\bin\Qt5Gui.dll "%CWD%\checkmate_win32_bin\bin\Qt5Gui.dll"
copy /Y C:\Qt\%QT%\%MSVC%\bin\Qt5Widgets.dll "%CWD%\checkmate_win32_bin\bin\Qt5Widgets.dll"
copy /Y C:\Qt\%QT%\%MSVC%\bin\Qt5Network.dll "%CWD%\checkmate_win32_bin\bin\Qt5Network.dll"
copy /Y C:\Qt\%QT%\%MSVC%\bin\..\plugins\platforms\qwindows.dll "%CWD%\checkmate_win32_bin\bin\platforms\qwindows.dll"
echo.
echo Building installer...
iscc checkmate_win32_bin\checkmate_setup.iss /ocheckmate_win32_bin\release /dSetupName=%SETUP% /dVersion=%VERSION% /dAppName=Checkmate
echo.
echo Build complete!
goto finish

:clean
echo.
echo Cleaning up binaries
cd "%CWD%\checkmate_win32_bin\bin"
if exist Checkmate.exe del Checkmate.exe
if exist Qt5Core.dll del Qt5Core.dll
if exist Qt5Gui.dll del Qt5Gui.dll
if exist Qt5Widgets.dll del Qt5Widgets.dll
if exist Qt5Network.dll del Qt5Network.dll
if exist platforms\qwindows.dll del platforms\qwindows.dll
if exist redist\vcredist_msvc2017.exe del redist\vcredist_msvc2017.exe
cd "%CWD%\checkmate_win32_bin"
rmdir /S /Q release
cd "%CWD%\src"
jom /J %NPROC% /F Makefile.Release clean
if exist object_script.Checkmate.Debug del object_script.Checkmate.Debug
if exist object_script.Checkmate.Release del object_script.Checkmate.Release
if exist debug rmdir /S /Q debug
if exist release rmdir /S /Q release
if exist Makefile del Makefile
if exist Makefile.Debug del Makefile.Debug
if exist Makefile.Release del Makefile.Release
cd ..
echo.
echo Cleaning complete
goto finish

:error
echo.
echo Error: "%1" is an invalid command switch!
echo.

:finish
rem Reset path variable to avoid broken duplicates
set PATH=%CPATH%
pause
