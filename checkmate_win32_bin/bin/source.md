# Checkmate Source

You've chosen to install the source code for Checkmate. Just look under the <APP_DIR>\src directory to view the source code.

## Building Source

No build scripts are given in the source bundle. Qt .pro project files are provided though, so you can build the source in Qt Creator, unless you know how to run Qmake and Jom.

## Required Libraries

If you compile Checkmate, you need the Qt .dll's to run the compiled exe. The following are he required .dll's:

* Qt5Core.dll
* Qt5Gui.dll
* Qt5Widgets.dll
* Qt5Network.dll
* platforms\qwindows.dll

## Full Source Code + Build Scripts

Fork the repo: [Checkmate GitLab Repo](https://gitlab.com/pazuzu156/Checkmate) All source and build scripts are in there.
